CREATE TABLE sales (
    order_id INT,
    customer_id INT,
    product_id INT,
    price DECIMAL(10,2),
    order_date DATE
);
